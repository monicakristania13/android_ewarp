package com.example.monicayan.ewarp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailSop {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tindakan")
    @Expose
    private String tindakan;
    @SerializedName("sop_id")
    @Expose
    private Integer sop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTindakan() {
        return tindakan;
    }

    public void setTindakan(String tindakan) {
        this.tindakan = tindakan;
    }

    public Integer getSop() {
        return sop;
    }

    public void setSop(Integer sop) {
        this.sop = sop;
    }
}
