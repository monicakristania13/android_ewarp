package com.example.monicayan.ewarp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.example.monicayan.ewarp.Model.LongsorBaru;
import com.example.monicayan.ewarp.Model.ResponseLongsorBaru;
import com.example.monicayan.ewarp.Service.Api;
import com.example.monicayan.ewarp.Service.GetService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LongsorSaatIni extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String id;
    LongsorBaru longsorbaruList;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longsor_saat_ini);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void getDataLongsor() {
        GetService service = Api.getClient().create(GetService.class);
        Call<ResponseLongsorBaru> call = (Call<ResponseLongsorBaru>) service.getResponseLongsorBaru();
        call.enqueue(new Callback<ResponseLongsorBaru>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLongsorBaru> call, @NonNull Response<ResponseLongsorBaru> response) {
                if (response.code() == 200){
                    assert response.body() != null;
                    longsorbaruList = response.body().getLongsorbaruList();
                    LatLng Pacitan = new LatLng(Double.parseDouble(((LongsorBaru) longsorbaruList).getLatitude()), Double.parseDouble(((LongsorBaru) longsorbaruList).getLongitude()));
                    mMap.addMarker(new MarkerOptions().position(Pacitan).title(longsorbaruList.getNamaKelurahan()).snippet("Status Perbaikan: " + longsorbaruList.getStatusLongbar()));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Pacitan, 18));
                    Log.d("longsorlist", response.body().getLongsorbaruList().toString());
                }
                else {
                    Toast.makeText(getApplicationContext(), response.code() + " : " +response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseLongsorBaru> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // jalanlankan rquest longsor terbaru
        getDataLongsor();
        // Add a marker ini taruh di onresponse nya
        // isi lat lang yang didapat dari hasil request server


    }
}