package com.example.monicayan.ewarp.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseLongsorBaru {
    @SerializedName("longsorbaruList")
    @Expose
    private LongsorBaru longsorbaruList;

    public LongsorBaru getLongsorbaruList() {
        return longsorbaruList;
    }

    public void setLongsorbaruList(LongsorBaru longsorbaruList) {
        this.longsorbaruList = longsorbaruList;
    }
}
