package com.example.monicayan.ewarp;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class MyPagerAdapter extends FragmentStatePagerAdapter  {
    public MyPagerAdapter(FragmentManager fm){

        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new SebelumLongsorFragment();
            case 1:
                return new SaatFragment();
            case 2:
                return new SesudahLongsorFragment();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 3;
    }
    @Override

    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Pra Longsor";
            case 1:
                return "Saat Longsor";
            case 2:
                return "Pasca Longsor";
            default:
                return null;
        }
    }
}
