package com.example.monicayan.ewarp;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.monicayan.ewarp.Adapter.SaatAdapter;
import com.example.monicayan.ewarp.Model.DetailSop;
import com.example.monicayan.ewarp.Model.ResponseSOP;
import com.example.monicayan.ewarp.Model.SOP;
import com.example.monicayan.ewarp.Service.Api;
import com.example.monicayan.ewarp.Service.GetService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 */
public class SaatFragment extends Fragment {
    String id;
    Intent intent;
    RecyclerView recyclerView;
    List<SOP> sopList;
    List<DetailSop> detailSops;
    SaatAdapter saatAdapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saat, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_saat);
        getDataBencana();
        return view;
    }

    private void getDataBencana() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        GetService service = Api.getClient().create(GetService.class);
        Call<ResponseSOP> call = (Call<ResponseSOP>) service.getResponseSOP();
        call.enqueue(new Callback<ResponseSOP>() {
            @Override
            public void onResponse(@NonNull Call<ResponseSOP> call, @NonNull Response<ResponseSOP> response) {
                if (response.code() == 200){
                    assert response.body() != null;
                    sopList = response.body().sopList;
                    Log.d("soplist", response.body().sopList.toString());
                    saatAdapter= new SaatAdapter(getContext(), sopList, 1);
                    recyclerView.setAdapter(saatAdapter);
                }
                else {
                    Toast.makeText(getContext(), response.code() + " : " +response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseSOP> call, @NonNull Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
