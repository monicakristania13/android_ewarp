package com.example.monicayan.ewarp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SOP {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nama_sop")
    @Expose
    private String namaSop;
    @SerializedName("detail_sop")
    @Expose
    private List<DetailSop> detailSop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamaSop() {
        return namaSop;
    }

    public void setNamaSop(String namaSop) {
        this.namaSop = namaSop;
    }

    public List<DetailSop> getDetailSop() {
        return detailSop;
    }

    public void setDetailSop(List<DetailSop> detailSop) {
        this.detailSop = detailSop;
    }
}
