package com.example.monicayan.ewarp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.monicayan.ewarp.Service.Api;
import com.example.monicayan.ewarp.Service.GetService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebaseUtil extends com.google.firebase.messaging.FirebaseMessagingService {

    public static void saveFirebaseToken() {
        FirebaseInstanceId
                .getInstance()
                .getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        Log.i("FCM", task.getResult().getToken());
                        if (task.isSuccessful()) {
                            if (task.getResult() != null) {
                                sendFirebaseToken(task.getResult().getToken());
                                Log.i("FCM", task.getResult().getToken());

                            }

                        }
                    }
                });
    }

    public static void sendFirebaseToken(final String token) {
        //buat update firebase_token baru
        GetService service = Api.getClient().create(GetService.class);
        Call<ResponseBody> call = service.saveToken(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    Log.i("save sukses", token);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public void onNewToken(String s) {
        Log.i("NewToken", s);
        super.onNewToken(s);
        sendFirebaseToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i("FCMes", remoteMessage.getData().toString());

        if (remoteMessage.getData() != null){
            Log.i("FCMes", remoteMessage.getData().toString());
            showNotification(remoteMessage.getData());
        }
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    private void showNotification(Map<String, String> data) {

        String title = data.get("title");
        String message = data.get("message");
        String channelId = "fcm_channel";

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setSubText(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId, "Longsor Notif", NotificationManager.IMPORTANCE_DEFAULT);

            notificationBuilder.setChannelId(channelId);

            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(channel);
            }
        }

        Notification notification = notificationBuilder.build();

        if (mNotificationManager != null) {
            mNotificationManager.notify(0, notification);
        }

    }
}
