package com.example.monicayan.ewarp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LongsorBaru {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kelurahan_id")
    @Expose
    private Integer kelurahanId;
    @SerializedName("tahun_longbar")
    @Expose
    private String tahunLongbar;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("status_longbar")
    @Expose
    private String statusLongbar;
    @SerializedName("kerusakan")
    @Expose
    private String kerusakan;
    @SerializedName("kerugian")
    @Expose
    private String kerugian;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("nama_kelurahan")
    @Expose
    private String nama_kelurahan;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKelurahanId() {
        return kelurahanId;
    }

    public void setKelurahanId(Integer kelurahanId) {
        this.kelurahanId = kelurahanId;
    }

    public String getTahunLongbar() {
        return tahunLongbar;
    }

    public void setTahunLongbar(String tahunLongbar) {
        this.tahunLongbar = tahunLongbar;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatusLongbar() {
        return statusLongbar;
    }

    public void setStatusLongbar(String statusLongbar) {
        this.statusLongbar = statusLongbar;
    }

    public String getKerusakan() {
        return kerusakan;
    }

    public void setKerusakan(String kerusakan) {
        this.kerusakan = kerusakan;
    }

    public String getKerugian() {
        return kerugian;
    }

    public void setKerugian(String kerugian) {
        this.kerugian = kerugian;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNamaKelurahan() {
        return nama_kelurahan;
    }

    public void setNamaKelurahan(String nama_kelurahan) {
        this.nama_kelurahan = nama_kelurahan;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
