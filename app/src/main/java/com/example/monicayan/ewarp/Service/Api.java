package com.example.monicayan.ewarp.Service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(){
        if (retrofit == null ){
//            String BASE_URL = "http://192.168.1.9:8000";
            String BASE_URL = "http://monica.irvansyah.id/";
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
