package com.example.monicayan.ewarp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monicayan.ewarp.Model.DetailSop;
import com.example.monicayan.ewarp.Model.SOP;
import com.example.monicayan.ewarp.R;

import java.util.List;


public class SaatAdapter extends RecyclerView.Adapter<SaatAdapter.SaatViewHolder> {
    private Context context;
    private List<DetailSop> detailSops;

    public SaatAdapter(Context context, List<SOP> sopList, int index) {
        this.context = context;
        this.detailSops = sopList.get(index).getDetailSop();
    }

    @NonNull
    public SaatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_saat, parent, false);
        return new SaatViewHolder(view);
    }

    public void onBindViewHolder(@NonNull SaatViewHolder holder, int position) {
        if (detailSops != null) {
            final DetailSop detailSop = detailSops.get(position);
            holder.id.setText(String.valueOf(detailSops.indexOf(detailSop) + 1));
            holder.tindakan.setText(detailSop.getTindakan());
        }
    }

    @Override
    public int getItemCount() {
        if (detailSops == null)
            return 0;
        else {
            return detailSops.size();
        }
    }

    class SaatViewHolder extends RecyclerView.ViewHolder{
        TextView id, tindakan;
        SaatViewHolder(View itemView){
            super(itemView);
            id = itemView.findViewById(R.id.id_saat);
            tindakan = itemView.findViewById(R.id.saat_bencana);
        }
    }
}