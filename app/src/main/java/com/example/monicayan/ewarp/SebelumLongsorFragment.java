package com.example.monicayan.ewarp;

import android.content.Intent;
import android.media.session.MediaSessionManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.monicayan.ewarp.Adapter.PraAdapter;
import com.example.monicayan.ewarp.Model.DetailSop;
import com.example.monicayan.ewarp.Model.ResponseSOP;
import com.example.monicayan.ewarp.Model.SOP;
import com.example.monicayan.ewarp.Service.Api;
import com.example.monicayan.ewarp.Service.GetService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 */
public class SebelumLongsorFragment extends Fragment {


    String id;
    Intent intent;
    RecyclerView recyclerView;
    List<SOP> sopList;
    List<DetailSop> detailSops;
    PraAdapter praAdapter = null;
    RecyclerView.LayoutManager layoutManager;
    MediaSessionManager manager;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sebelum_longsor, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_sebelum);
        getDataPra();
        return view;
    }

    private void getDataPra() {
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        GetService service = Api.getClient().create(GetService.class);
        Call<ResponseSOP> call = service.getResponseSOP();
        call.enqueue(new Callback<ResponseSOP>() {
            @Override
            public void onResponse(@NonNull Call<ResponseSOP> call, @NonNull Response<ResponseSOP> response) {
                if (response.code() == 200){
                    assert response.body() != null;
                    sopList = response.body().sopList;
                    praAdapter= new PraAdapter(view.getContext(), sopList, 0);
                    recyclerView.setAdapter(praAdapter);
                }
                else {
                    Toast.makeText(view.getContext(), response.code() + " : " +response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseSOP> call, @NonNull Throwable t) {
                Toast.makeText(view.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
