package com.example.monicayan.ewarp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.cardview.widget.CardView;

import com.example.monicayan.ewarp.Service.Api;
import com.example.monicayan.ewarp.Service.GetService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseApp;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Button button, button2, button3, btnPacitan, sopLongsor, tentang;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button = (Button) findViewById(R.id.btnSaatini);
        button2 = (Button) findViewById(R.id.btnHistory);
        button3 = (Button) findViewById(R.id.btnRawan);
        sopLongsor = findViewById(R.id.btnSop);
        tentang = findViewById(R.id.btnAbout);
        btnPacitan = findViewById(R.id.btnPacitan);
        Log.v("TEST", "coba");

        FirebaseUtil.saveFirebaseToken();

        btnPacitan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://bpbd.pacitankab.go.id/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLongsorSaatIni();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRiwayatLongsor();
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRawanLongsor();
            }
        });
        sopLongsor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSopLongsor();
            }
        });
        tentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTentang();
            }
        });

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Log.d("location", "onsuccess");
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            try {
                                List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                                if (addressList != null && addressList.size() > 0) {
                                    Address address = addressList.get(0);
                                    //StringBuilder sb = new StringBuilder();
                                    String kec = address.getLocality();
                                    String kel = address.getSubLocality();

                                    Log.d("locationkec", address.getLocality());
                                    Log.d("locationkel", address.getSubLocality());
                                    getRawan(kec, kel);
                                } else {
                                    Log.d("locationaddress", "kosong");
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            // Logic to handle location object
                        }
                    }
                });

    }

    private void getRawan(String kec, String kel) {
        GetService service = Api.getClient().create(GetService.class);
        Call<ResponseBody> call = service.getRawanLongsor(kec, kel);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null){

                        Log.d("locationsend", "Sukses");
                    }


                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


            }

        });
    }

    private void openSopLongsor() {
        Intent intent = new Intent(this, SopActivity.class);
        startActivity(intent);
    }

    public void openLongsorSaatIni() {
        Intent intent = new Intent(this, LongsorSaatIni.class);
        startActivity(intent);
    }

    public void openRiwayatLongsor() {
        Intent intent = new Intent(this, RiwayatLongsor.class);
        startActivity(intent);
    }

    public void openRawanLongsor() {
        Intent intent = new Intent(this, RawanLongsor.class);
        startActivity(intent);
    }

    private void openTentang() {
        Intent intent = new Intent(this, TentangActivity.class);
        startActivity(intent);
    }
}

