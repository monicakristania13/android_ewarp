package com.example.monicayan.ewarp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monicayan.ewarp.Model.DetailSop;
import com.example.monicayan.ewarp.Model.SOP;
import com.example.monicayan.ewarp.R;

import java.util.List;


public class PascaAdapter extends RecyclerView.Adapter<PascaAdapter.PascaViewHolder> {
    private Context context;
    private List<DetailSop> detailSops;

    public PascaAdapter(Context context, List<SOP> sopList, int index) {
        this.context = context;
        this.detailSops = sopList.get(index).getDetailSop();
    }

    @NonNull
    public PascaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_sesudah, parent, false);
        return new PascaViewHolder(view);
    }

    public void onBindViewHolder(@NonNull PascaViewHolder holder, int position) {
        if (detailSops != null) {
            final DetailSop detailSop = detailSops.get(position);
            holder.id.setText(String.valueOf(detailSops.indexOf(detailSop) + 1));
            holder.tindakan.setText(detailSop.getTindakan());
        }
    }

    @Override
    public int getItemCount() {
        if (detailSops == null)
            return 0;
        else {
            return detailSops.size();
        }
    }

    class PascaViewHolder extends RecyclerView.ViewHolder{
        TextView id, tindakan;
        PascaViewHolder(View itemView){
            super(itemView);
            id = itemView.findViewById(R.id.id_pasca);
            tindakan = itemView.findViewById(R.id.pasca_bencana);
        }
    }
}
