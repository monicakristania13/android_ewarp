package com.example.monicayan.ewarp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monicayan.ewarp.Model.DetailSop;
import com.example.monicayan.ewarp.Model.SOP;
import com.example.monicayan.ewarp.R;

import java.util.List;


public class PraAdapter extends RecyclerView.Adapter<PraAdapter.PraViewHolder> {
    private Context context;
    private List<DetailSop> detailSops;

    public PraAdapter(Context context, List<SOP> sopList, int index) {
        this.context = context;
        this.detailSops = sopList.get(index).getDetailSop();
    }

    @NonNull
    public PraViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_sebelum, parent, false);
        return new PraViewHolder(view);
    }

    public void onBindViewHolder(@NonNull PraViewHolder holder, int position) {
        if (detailSops != null) {
            final DetailSop detailSop = detailSops.get(position);
            holder.id.setText(String.valueOf(detailSops.indexOf(detailSop) + 1));
            holder.tindakan.setText(detailSop.getTindakan());
        }
    }

    @Override
    public int getItemCount() {
        if (detailSops == null)
            return 0;
        else {
            return detailSops.size();
        }
    }

    class PraViewHolder extends RecyclerView.ViewHolder{
        TextView id, tindakan;
        PraViewHolder(View itemView){
            super(itemView);
            id = itemView.findViewById(R.id.id_pra);
            tindakan = itemView.findViewById(R.id.pra_bencana);
        }
    }
}
