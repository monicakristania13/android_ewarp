package com.example.monicayan.ewarp.Service;

import com.example.monicayan.ewarp.Model.ResponseLongsorBaru;
import com.example.monicayan.ewarp.Model.ResponseSOP;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetService {
    @GET("/api/sop")
    Call<ResponseSOP> getResponseSOP();

    @GET("/api/longsorbaru")
    Call<ResponseLongsorBaru> getResponseLongsorBaru();

    @FormUrlEncoded
    @POST("/api/savetoken")
    Call<ResponseBody> saveToken(@Field("token") String token);

    @GET("/map/rawanlongsor")
    Call<ResponseBody> getRawanLongsor(@Query("kec") String kecamatan,
                                       @Query("kel") String kelurahan);
}
